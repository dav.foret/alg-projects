#include <vector>
#include <iostream>
#include <string>
#include <sstream>
#include <queue>
#include "utils.hpp"

int M;
int N;
int E_INIT;
int D_MOV;
int D_ENV;
int MIN_MOVES = INT_MAX;
int DIRECTIONS[4][2] = {{1,0},{-1,0},{0,1},{0,-1}};

int main()
{
/////////////////////////////////////////////////SETUP//////////////////////
	const rlimit limit = {1000000000, 1000000000};
	setrlimit(RLIMIT_STACK, &limit);
	std::queue<Node> queue;
	std::vector<std::vector<int>> env_ionization;
	std::vector<std::vector<std::vector<int>>> min_moves_cube;
	std::string line;
	std::getline(std::cin, line);
	std::stringstream ss(line);
	ss >> M;
	ss >> N;
	ss >> E_INIT;
	ss >> D_MOV;
	ss >> D_ENV;
	env_ionization.resize(N);
	min_moves_cube.resize(N);
	int temp;
	int k = 0;

	while(std::getline(std::cin, line)) {
		std::stringstream ss2(line);

		for(int i = 0; i < N; ++i) {
			ss2 >> temp;
			env_ionization[i].push_back(temp);
			min_moves_cube[i].resize(800);

			for(int n = 0; n < 100; ++n) {
				min_moves_cube[i][k].push_back(INT_MAX);
			};
		};
		++k;
	};
////////////////////////////////////////////SETUP//////////////////////////////
	Node start = {0, 0, E_INIT, 0};
	queue.push(start);
	min_moves_cube[0][0][E_INIT] = 0;
	recursion(env_ionization, min_moves_cube, queue);
	std::cout << MIN_MOVES << '\n';
};