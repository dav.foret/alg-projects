#include <vector>
#include <iostream>
#include <string>
#include <sstream>
#include <queue>
#include <sys/resource.h>
#include <climits>

extern int M;
extern int N;
extern int E_INIT;
extern int D_MOV;
extern int D_ENV;
extern int MIN_MOVES;

extern int DIRECTIONS[4][2];

typedef struct node {
	int x_pos;
	int y_pos;
	int ionization;
	int moves;
} Node;

void recursion(std::vector<std::vector<int>>& env_ionization, std::vector<std::vector<std::vector<int>>>& min_moves_cube, std::queue<Node>& queue);