#include "utils.hpp"

void recursion(std::vector<std::vector<int>>& env_ionization, std::vector<std::vector<std::vector<int>>>& min_moves_cube, std::queue<Node>& queue) {
	if(queue.empty()) return;
	
	int current_x_pos = queue.front().x_pos;
	int current_y_pos = queue.front().y_pos;
	int current_ionization = queue.front().ionization;
	int current_moves = queue.front().moves;
	//Terminate if the current state satisfies conditions
	if(current_x_pos == N - 1 && current_y_pos == M - 1) {
		if(current_moves < MIN_MOVES) {
			MIN_MOVES = current_moves;
			
		};
		queue.pop();
		recursion(env_ionization, min_moves_cube, queue);
		return;
	};
	//Some basic heuristic 1. - prefer going in directions that
	if(current_moves == MIN_MOVES) {
		queue.pop();
		recursion(env_ionization, min_moves_cube, queue);
		return;
	};
	//Check states that would result after executing every possible move, adding them to queue if current_moves + 1 < min_moves_cube[x][y][z], re-writing if neccessary
	for(int i = 0; i < 4; ++i) {
		//Check bounds
		int d_x = DIRECTIONS[i][0] + current_x_pos;
		int d_y = DIRECTIONS[i][1] + current_y_pos;

		if(d_x < 0 || d_x >= N || d_y < 0 || d_y >= M) {
			continue;
		};
		//Calculate resulting state, reject if ionization would fall under 1
		int new_ionization = current_ionization;
        int I1 = env_ionization[current_x_pos][current_y_pos];
        int I2 = env_ionization[d_x][d_y];

        if(I1 < I2) {
        	new_ionization += D_MOV;
        } else if(I1 > I2) {
        	if(current_ionization < D_MOV) {
        		new_ionization = 0;
        	} else {
        		new_ionization -= D_MOV;
        	};
        };

		if(new_ionization >= D_ENV + I2) {
			new_ionization -= D_ENV;
		} else if (D_ENV + I2 > new_ionization && new_ionization > I2) {
			new_ionization = I2;
		};
		if(new_ionization < 0) continue;
		if(new_ionization == 0 && (d_x != N - 1 || d_y != M - 1)) continue;
		//Reject if we were already in the resulting state (i.e. same position, same energy, smaller amount of moves
		//means best path can not be found by checking that node); also prevents loops
		if(current_moves + 1>= min_moves_cube[d_x][d_y][new_ionization]) {
			continue;
		};
		//Some basic heuristic 2.

		//Rewrite the state cube, as the state WILL be examined, with appropriate "time" = moves
		min_moves_cube[d_x][d_y][new_ionization] = current_moves + 1;
		//Push the state into the queue with incremented moves
		Node new_node = {d_x, d_y, new_ionization, current_moves + 1};
		queue.push(new_node);
	};
	queue.pop();
	recursion(env_ionization, min_moves_cube, queue);
};