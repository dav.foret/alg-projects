#include <iostream>
#include <string>
#include <deque>
#include <vector>
#include <sstream>
#include <map>
#include <ctime>

#define DEBUG 0
//TODO: switch to vector columns
/*
*Program stores notes by columns into a std::vector of std::deque structures. 
*Consecutive elements of every std::deque (column) represent visible part of id-identified note, with height value.
*
*Thus, insertion is done from front - any columns of notes overlapped are popped (i.e. any element with height value smaller than added column).
*As such, after all notes are added, the data structure effectively contains every note visible from front, by column.
*
*Additionally, anytime a column element is added into a std::deque that is empty for a first time while processing one given note, counter of back-*visible notes is increased by one.
*
*Amount of front visible notes is obtained by iterating through the main std::vector, taking note of every unique key from all elements of *std::deque structures.
*/

typedef struct {int key; int height;} NotesKey;
typedef struct {int key; int height; int width; int offset;} BaseKey;
typedef int vis[2];

int main() {
    std::string line;
    //Load note-space params
    std::getline(std::cin, line);
    std::vector<int> space_params;
    std::stringstream s_stream(line);
    int param;
    int max_free_id = 0;
    int note_count = 0;

    #if DEBUG
    std::clock_t start;
    double duration;
    start = std::clock();
    #endif

    while(s_stream >> param) {
        space_params.push_back(param);
    } 
    //Set-up data structure for visibility tracking - [0]fwd, [1] bwd
    note_count = space_params[1];
    vis vis_map[note_count];

    for(int i = 0; i < note_count; ++i) {
    	vis_map[i][0] = 0;
    	vis_map[i][1] = 0;	
    }
    //Vector holding columns
    std::vector<std::vector<BaseKey>> base(space_params[0]);
    //Vector holding ID and height of highest note in every column
    std::vector<NotesKey> highest_notes(space_params[0]);
    //Vector holding all notes, for checking forward vis
    std::vector<BaseKey> notes(note_count);

    //Preallocing base
    for(auto&& it = base.begin(); it != base.end(); ++it) {
    	it->reserve(20);
    }

    for(auto&& it = highest_notes.begin(); it != highest_notes.end(); ++it) {
    	//TODO: make sure note vis checking process can deal with -1 key for empty columns
    	it->key = -1;
    	it->height = 0;	
    }
    //Input loop
    while(std::getline(std::cin, line)) {
		//[0] - x offset, [1] - height, [2] - width
        std::vector<int> note_params;
        std::stringstream ss(line);
        
        while(ss >> param) {
            note_params.push_back(param);
        } 
        //For checking visibility from back
        BaseKey column_element = {max_free_id, note_params[1], note_params[2], note_params[0]};
        ++max_free_id;
	    //Add note info to column where it begins
	    base[note_params[0]].push_back(column_element);
	    notes.push_back(column_element);
	    //For every column relevant column, check if there is a higher note, modify backwards visibility and column highest info if so
	    for(int i = note_params[0]; i < note_params[0] + note_params[2]; ++i) {
       		if(highest_notes[i].height < column_element.height) {
       			highest_notes[i].height = column_element.height;
       			highest_notes[i].key = column_element.key;
       			vis_map[column_element.key][1] = 1;
       		}
	    }
    }
    #if DEBUG
    duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;
    std::cout<<"After adding notes clock is: "<< duration <<'\n';
    #endif
    //Reset highest notes
    for(auto&& it = highest_notes.begin(); it != highest_notes.end(); ++it) {
    	//TODO: make sure note vis checking process can deal with -1 key for empty columns
    	it->key = -1;
    	it->height = 0;	
    }
    //Process vector of notes, going backwards - set highest notes as in while adding notes
    for(auto&& curr_note = notes.rbegin(); curr_note != notes.rend(); ++curr_note) {
    	for(int curr_column = curr_note->offset; curr_column < curr_note->offset + curr_note->width; ++curr_column) {
    		if(highest_notes[curr_column].height < curr_note->height) {
    			vis_map[curr_note->key][0] = 1;
    			highest_notes[curr_column].height = curr_note->height;
    		}
    	}
    }
    //Count vismap elements with 1,1 and sum of 0,1 and 1,0; lastly of 0,0 elements
    int both, fwd, bwd, none;
    both = fwd = bwd = none = 0;
    for(int i = 0; i < note_count; ++i) {
    	if(vis_map[i][0] == 0) {
    		if(vis_map[i][1] == 0) {
    			++none;
    		} else {
    			++bwd;
    		}
    	} else {
    		if(vis_map[i][1] == 0) {
    			++fwd;
    		} else {
    			++both;
    		}
    	}
    }

    #if DEBUG
    std::cout << both << " " << bwd << " " << fwd << " " << none << "\n";
    #endif

    std::cout << both << " " << bwd + fwd << " " << none << "\n";
    return 0;
}