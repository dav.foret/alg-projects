#include <vector>
#include <string>
#include <iostream>
#include <sstream>

class Experiment {
public:
	int _score;
	int _duration;
	std::vector<int> _days;
};

class Laboratory {
public:
	int _max_score = 0;
	int _max_used_days = 0;
	int _current_score = 0;
	int _current_used_days = 0;
	std::vector<Experiment> _roster;
	std::vector<int> _schedule;
	std::vector<Experiment> _experiments;
	std::vector<Experiment>::reverse_iterator _exp_iter;

	int _fits_schedule(Experiment exp, int offset);
	void _pop_from_schedule(int offset);
};

void recursion(Laboratory& lab);

int load_data(Laboratory& lab);

