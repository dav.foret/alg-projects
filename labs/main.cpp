#include <iostream>
#include "utils.h"

int main(int argc, char const *argv[])
{
	Laboratory lab;
	load_data(lab);
	 recursion(lab);
	
	std::cout << lab._max_score << " " << lab._max_used_days << "\n";
	/*for(auto it = lab._experiments.begin(); it != lab._experiments.end(); ++it) {
		std::cout << it->_score << "\n";
	}*/
	return 0;
}