#include "utils.h"

void recursion(Laboratory& lab) {
	auto& iterator = lab._exp_iter;

	if(lab._exp_iter == lab._experiments.rend()) {
		return;
	}
	//Try all possible positions
	for(int i = 0; i < lab._schedule.size() - 1; ++i) {

		if(i + iterator->_days.back() >= lab._schedule.size()) {
			break;
		}
		if(lab._fits_schedule(*iterator, i)) {
			++iterator;
			recursion(lab);
			--iterator;
			lab._pop_from_schedule(i);
		}
	}
	++iterator;
	recursion(lab);
	--iterator;
	return;
}

//Success = 1
int Laboratory::_fits_schedule(Experiment exp, int offset) {

	for(auto it = exp._days.begin(); it != exp._days.end(); ++it) {
		if(this->_schedule[*it + offset] == 1) {
			return 0;
		}	
	}
	for(auto it = exp._days.begin(); it != exp._days.end(); ++it) {
		this->_schedule[*it + offset] = 1;
		++this->_current_used_days;
	}	
	this->_roster.push_back(exp);
	this->_current_score += exp._score;
	
	if(this->_current_score > this->_max_score) {
		this->_max_score = this->_current_score;
		this->_max_used_days = this->_current_used_days;
	}
	return 1;
}

void Laboratory::_pop_from_schedule(int offset) {
	for(auto it = this->_roster.back()._days.rbegin(); it != this->_roster.back()._days.rend(); ++it) {
		this->_schedule[*it + offset] = 0;
		--this->_current_used_days;
	}	
	this->_current_score -= this->_roster.back()._score;
	this->_roster.pop_back();
}

int load_data(Laboratory& lab) {
	int days = 0;
	int experiments = 0;
	std::string line;
    std::getline(std::cin, line);
    std::stringstream s_stream(line);
    s_stream >> experiments;
    s_stream >> days;

    while(std::getline(std::cin, line)) {
    	Experiment exp;
    	std::stringstream ss1(line);
    	ss1 >> exp._duration;
    	ss1 >> exp._score;

    	std::getline(std::cin, line);
    	std::stringstream ss2(line);
    	int day_id;

    	while(ss2 >> day_id) {
    		exp._days.push_back(day_id);
    		//std::cout << day_id << "\n";
    	}
    	lab._experiments.push_back(exp);
    }
    lab._schedule.resize(days);
    lab._exp_iter = lab._experiments.rbegin();
    return 1;
}	