#include <iostream>
#include "utils.hpp"
 
int main(int argc, char const *argv[])
{
	unsigned int pilot_weight;
	long long current_diff = 0;
	long long max_diff = LLONG_MAX;
	int optimal_leaf = -1;
	RecursionVector data(load_data(&pilot_weight));
	Tree tree;
	tree.root = recursion(tree, data, 0, data.storage_plane.size() - 1);
	std::cout << sum_weight_diffs(tree, tree.root) << ' ';
	add_pilot(tree, pilot_weight, tree.root, current_diff, max_diff, optimal_leaf);
	update_tree(tree, tree.root, optimal_leaf, pilot_weight);
	current_diff = 0;
	max_diff = LLONG_MAX;
	add_pilot(tree, pilot_weight, tree.root, current_diff, max_diff, optimal_leaf);
	update_tree(tree, tree.root, optimal_leaf, pilot_weight);
	std::cout << sum_weight_diffs(tree, tree.root) << '\n';
    return 0;
}