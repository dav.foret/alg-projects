#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <cmath>
#include <cstring>
#include <sys/resource.h>
#include <unistd.h>
 
class Node {
public:
    Node(int key): _key(key) {};
    Node() {};
 
    int _depth = 0;
    int _l_depth = -1;
    int _r_depth = -1;
    int _l_child = -1;
    int _r_child = -1;
    int _key = -1;  
};
 
class Tree {
public:
    int _nodes_count = 0;
    int _root = 0;
    Node _nodes[61000];
 
    void _add_key(int key);
    void _remove_key(int key);
};
 
void inorder(Tree tree, int index);