int EXISTING_KEYS[610000] = {0};
extern int MIN_FREE_INDEX;
extern int MAX_DEPTH;
extern int CONSOLIDATIONS_AMOUNT;
 
int main() {
    const rlimit limit = {1000000000, 1000000000};
    setrlimit(RLIMIT_STACK, &limit);
    int N;
    int key;
    std::string line;
    std::getline(std::cin, line);
    std::stringstream sstream(line);
    sstream >> N;
    std::getline(std::cin, line);
    std::stringstream sstream2(line);
    std::vector<int> keys;
    keys.reserve(N);
    Tree tree;
    while(sstream2 >> key) {
        keys.push_back(key);
    };
 
    for(auto&& it = keys.begin(); it != keys.end(); ++it) {
        key = *it;
        //std::cout << "\n\n Processing command: " << key << "\n\n";
        if(key > 0) {
            if(!EXISTING_KEYS[key]) {
                EXISTING_KEYS[key] = 1;
                tree._add_key(key);
            };
        } else {
 
            if(EXISTING_KEYS[abs(key)]) {
                EXISTING_KEYS[abs(key)] = 0;
                tree._remove_key(abs(key)); 
            };
        };
        /*
        for(int i = 0; i < MIN_FREE_INDEX; ++i) {
            int l_placeholder;
            int r_placeholder;
 
            if(tree._nodes[i]._l_child == -1) {
                l_placeholder = -1;
            } else {
                l_placeholder = tree._nodes[tree._nodes[i]._l_child]._key;
            };
            if(tree._nodes[i]._r_child == -1) {
                r_placeholder = -1;
            } else {
                r_placeholder = tree._nodes[tree._nodes[i]._r_child]._key;
            };
            std::cout << "depth: "<<tree._nodes[i]._depth << " key: "<<tree._nodes[i]._key << " " << l_placeholder << " " << r_placeholder;
            std::cout << '\n';
        };
        std::cout << "----------------------------------------------------\n";
        */
    };
    int depth;
    (tree._root == -1) ? depth = -1 : depth = tree._nodes[tree._root]._depth;
    std::cout << CONSOLIDATIONS_AMOUNT << ' ' << depth << '\n';
};