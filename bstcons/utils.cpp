#include "utils.hpp"
 
extern int* EXISTING_KEYS;
int MIN_FREE_INDEX = 0;
int MAX_DEPTH = 0;
int CONSOLIDATIONS_AMOUNT = 0;
 
void inorder(Tree& tree, int curr_index, std::vector<int>& storage) {
    Node& curr_node = tree._nodes[curr_index];
    int curr_key = curr_node._key;
    int& r_child = curr_node._r_child;
    int& l_child = curr_node._l_child;
 
    if(l_child != -1) inorder(tree, l_child, storage);
    storage.push_back(curr_key);
 
    if(r_child != -1) inorder(tree, r_child, storage);
};
 
int find_replacement(Tree& tree, int curr_index) {
    Node& curr_node = tree._nodes[curr_index];
    int& curr_key = curr_node._key;
    int& r_child = curr_node._r_child;
    int& l_child = curr_node._l_child;
    int ret;
    //Current node is the rightmost
    if(r_child == -1) {
        return curr_index;
 
    } else {
        ret = find_replacement(tree, r_child);
        //If the found replacement is current right child, inherit any possible left child
        if(ret == r_child) {
            r_child = tree._nodes[ret]._l_child;
        };
    };
    //Check current height
    (l_child != -1) ? curr_node._l_depth = tree._nodes[l_child]._depth : curr_node._l_depth = -1;
    (r_child != -1) ? curr_node._r_depth = tree._nodes[r_child]._depth : curr_node._r_depth = -1;
    (curr_node._l_depth > curr_node._r_depth) ? curr_node._depth = curr_node._l_depth + 1 : curr_node._depth = curr_node._r_depth + 1;
    return ret;
};
 
int recursive_remove(int key, Tree& tree, int curr_index) {
 
    Node& curr_node = tree._nodes[curr_index];
    int& curr_key = curr_node._key;
    int& r_child = curr_node._r_child;
    int& l_child = curr_node._l_child;
    //if(key == 273196) std::cout << "key: " << curr_key <<'\n';
    //usleep(200000);
    if(curr_index == -1) return -1;
 
    if(key == curr_key) {
        --tree._nodes_count;
        //Only left child exists
        if(l_child != -1 && r_child == -1) {
            return l_child;
        }
        //Only right child exists
        else if(l_child == -1 && r_child != -1) {
            return r_child;
        //Both children exist
        } else if(l_child != -1 && r_child != -1) {
 
            int r_id = find_replacement(tree, l_child);
            Node replacement = tree._nodes[r_id];
            curr_key = replacement._key;
            //In case the replacement node is child of the calling node set l_child to replacement's left subtree (went as right as possible)
            if(r_id == l_child) {
                l_child = replacement._l_child;
            };
        //No children exist
        } else {
            return -1;
        };
 
    } else if(key > curr_key) {
        r_child = recursive_remove(key, tree, r_child);
    } else {
        l_child = recursive_remove(key, tree, l_child);
    };
    //Update depth
    (l_child != -1) ? curr_node._l_depth = tree._nodes[l_child]._depth : curr_node._l_depth = -1;
    (r_child != -1) ? curr_node._r_depth = tree._nodes[r_child]._depth : curr_node._r_depth = -1;
    (curr_node._l_depth > curr_node._r_depth) ? curr_node._depth = curr_node._l_depth + 1 : curr_node._depth = curr_node._r_depth + 1;
    return curr_index;
};
 
int add_child(int key, Node* nodes, int& nodes_count) {
    //Reset values of node table entry
    ++nodes_count;
    nodes[MIN_FREE_INDEX]._r_child = -1;
    nodes[MIN_FREE_INDEX]._l_child = -1;
    nodes[MIN_FREE_INDEX]._depth = 0;
    nodes[MIN_FREE_INDEX]._r_depth = -1;
    nodes[MIN_FREE_INDEX]._l_depth = -1;
    nodes[MIN_FREE_INDEX]._key = key;
    ++MIN_FREE_INDEX;
    return MIN_FREE_INDEX - 1;
};
 
void recursive_add(int key, Tree& tree, int curr_index) {
    //Empty tree
    if(!tree._nodes_count) {
        tree._root = 0;
        MIN_FREE_INDEX = 0;
        add_child(key, tree._nodes, tree._nodes_count);
        return;
    };
    Node& curr_node = tree._nodes[curr_index];
    int& curr_key = curr_node._key;
    int& r_child = curr_node._r_child;
    int& l_child = curr_node._l_child;
 
    if(key > curr_key) {
        if(r_child == -1) {
            r_child = add_child(key, tree._nodes, tree._nodes_count);
        } else {
            recursive_add(key, tree, r_child);
        }
    } else {
        if(l_child == -1) {
            l_child = add_child(key, tree._nodes, tree._nodes_count);
        } else {
            recursive_add(key, tree, l_child);
        }
    };
    //Update depth
    (l_child != -1) ? curr_node._l_depth = tree._nodes[l_child]._depth : curr_node._l_depth = -1;
    (r_child != -1) ? curr_node._r_depth = tree._nodes[r_child]._depth : curr_node._r_depth = -1;
    (curr_node._l_depth > curr_node._r_depth) ? curr_node._depth = curr_node._l_depth + 1 : curr_node._depth = curr_node._r_depth + 1;
};
 
int recursive_consolidation(Tree& tree, std::vector<int>& storage, int left_bound, int right_bound) {
    ++MIN_FREE_INDEX;
    ++tree._nodes_count;
    int ret = MIN_FREE_INDEX;
    int h;
    int subtree_size = right_bound - left_bound + 1;
    Node& curr_node = tree._nodes[ret];
    int& curr_key = curr_node._key;
    int& r_child = curr_node._r_child;
    int& l_child = curr_node._l_child;
 
    //Handle special cases
    if(subtree_size == 1) {
        curr_key = storage[right_bound];
        curr_node._depth = 0;
        curr_node._l_depth = -1;
        curr_node._r_depth = -1;
        l_child = -1;
        r_child = -1;
        return ret;
 
    } else if(subtree_size == 2) {
        curr_key = storage[right_bound];
        r_child = -1;
        curr_node._l_depth = 0;
        curr_node._r_depth = -1;
        curr_node._depth = 1;
 
        ++MIN_FREE_INDEX;
        l_child = MIN_FREE_INDEX;
 
        Node& other_node = tree._nodes[MIN_FREE_INDEX];
        other_node._key = storage[left_bound];
        other_node._l_child = -1;
        other_node._r_child = -1;
        other_node._depth = 0;
        other_node._l_depth = -1;
        other_node._r_depth = -1;
        ++tree._nodes_count;
        return ret;
    };
 
    int l_left_bound;
    int l_right_bound;
    int r_left_bound;
    int r_right_bound;
    //Find height of L and R subtree, if remaning keys fill up a whole h+1 height of L give rest to R subtree
    for(h = 0;; ++h) {
        //Subtrees of height h would be too large, decrement h and divide key storage
        if((pow(2, h + 1) - 1)*2 + 1 > subtree_size) {
            --h;
            //Too many remaining keys for 2^(h+1) depth, give some to R subtree
            if(subtree_size - (pow(2, h + 1) - 1)*2 - 1 > pow(2, h + 1)) {
                l_left_bound = left_bound;
                l_right_bound = left_bound + (pow(2, h + 1) - 1) - 1 + pow(2, h + 1);
                r_left_bound = l_right_bound + 2;
                r_right_bound = right_bound;
                break;
            } else {
                l_left_bound = left_bound;
                r_left_bound = right_bound - (pow(2, h + 1) - 1) + 1;
                l_right_bound = r_left_bound - 2;
                r_right_bound = right_bound;
                break;
            };
        };
    };
    //Found root of current subtree
    curr_key = storage[r_left_bound - 1];
    //Update depth
    l_child = recursive_consolidation(tree, storage, l_left_bound, l_right_bound);
    r_child = recursive_consolidation(tree, storage, r_left_bound, r_right_bound);
    (l_child != -1) ? curr_node._l_depth = tree._nodes[l_child]._depth : curr_node._l_depth = -1;
    (r_child != -1) ? curr_node._r_depth = tree._nodes[r_child]._depth : curr_node._r_depth = -1;
    (curr_node._l_depth > curr_node._r_depth) ? curr_node._depth = curr_node._l_depth + 1 : curr_node._depth = curr_node._r_depth + 1;
    return ret;
};
 
void consolidate(Tree& tree) {
    //std::cout << "Consolidating\n";
    ++CONSOLIDATIONS_AMOUNT;
    //Contains tree keys sorted inorder
    std::vector<int> storage;
    storage.reserve(60000);
    MIN_FREE_INDEX = -1;
    tree._nodes_count = 0;
    inorder(tree, 0, storage);
    recursive_consolidation(tree, storage, 0, storage.size() - 1);
    ++MIN_FREE_INDEX;
    tree._root = 0;
};
 
void Tree::_add_key(int key) {
    recursive_add(key, *this, _root);
    //std::cout << "\n\n2^h and nodes * 2: "<<(pow(2, _nodes[_root]._depth)) << ' ' << 2*_nodes_count <<" root: "<< _root << "\n\n";
    if(pow(2, _nodes[_root]._depth) > 2*_nodes_count && _nodes_count != 0) consolidate(*this);
};
 
void Tree::_remove_key(int key) {
    _root = recursive_remove(key, *this, _root);
    //std::cout << "\n\n2^h and nodes * 2: "<<(pow(2, _nodes[_root]._depth)) << ' ' << 2*_nodes_count <<" root: "<< _root << "\n\n";
    if(pow(2, _nodes[_root]._depth) > 2*_nodes_count && _nodes_count != 0) consolidate(*this);
};